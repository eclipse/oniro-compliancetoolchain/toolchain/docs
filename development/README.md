---
SPDX-License-Identifier: CC-BY-SA-4.0 AND LicenseRef-Trademark-Notice
SPDX-FileCopyrightText: NOI Techpark SpA
---

# Documentation about Oniro Compliance Toolchain development

- [Getting started](../deployment/getting_started.md)
- [Set up a development playground](./setup_dev_playground)
- Detail documentation may be found in each project's README:
	- [TinfoilHat](https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/toolchain/tinfoilhat)
	- [Aliens4Friends](https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/toolchain/aliens4friends)
	- [Dashboard](https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/toolchain/dashboard)
	- [CI Pipelines](https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/toolchain/pipelines)
- [BANG integration](./bang)
- [Fossology integration](./fossology)
- [SW360 integration](./sw360)
