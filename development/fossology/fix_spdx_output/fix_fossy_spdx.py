#!/usr/bin/env python3
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Alberto Pianon <pianon@array.eu>

import os
import sys

from spdx.document import License as SPDXLicense
from spdx.parsers.tagvalue import Parser as SPDXTagValueParser
from spdx.parsers.tagvaluebuilders import Builder as SPDXTagValueBuilder
from spdx.writers.tagvalue import write_document as tv_write_document

GPL_RENAME = {
	"GPL-1.0" : "GPL-1.0-only",
	"GPL-1.0+" : "GPL-1.0-or-later",
	"GPL-2.0" : "GPL-2.0-only",
	"GPL-2.0+" : "GPL-2.0-or-later",
	"GPL-3.0" : "GPL-3.0-only",
	"GPL-3.0+" : "GPL-3.0-or-later",
	"LGPL-2.0" : "LGPL-2.0-only",
	"LGPL-2.0+" : "LGPL-2.0-or-later",
	"LGPL-2.1" : "LGPL-2.1-only",
	"LGPL-2.1+" : "LGPL-2.1-or-later",
	"LGPL-3.0" : "LGPL-3.0-only",
	"LGPL-3.0+" : "LGPL-3.0-or-later",
	"LicenseRef-LPGL-2.1-or-later": "LicenseRef-LGPL-2.1-or-later",
    "LicenseRef-GPL-2.0-or-late": "LicenseRef-GPL-2.0-or-later",
}

GPL_LICS = [
	"GPL-1.0-only",
	"GPL-1.0-or-later",
	"GPL-2.0-only",
	"GPL-2.0-or-later",
	"GPL-3.0-only",
	"GPL-3.0-or-later",
	"LGPL-2.0-only",
	"LGPL-2.0-or-later",
	"LGPL-2.1-only",
	"LGPL-2.1-or-later",
	"LGPL-3.0-only",
	"LGPL-3.0-or-later",
	"LPGL-2.1-or-later", # bugfix, just to remove misspelled extracted_licenses
    "GPL-2.0-or-late",
]

TO_REMOVE = [
    "LicenseRef-No_license_found"
]

class SPDXLogger:
	def log(self, _):
		pass # do not log errors, they are returned by parse method


def fix_fossy_lic(license):
    if not isinstance(license, SPDXLicense):
        return license
    identifier = license.identifier
    identifier = identifier.replace("LicenseRef-LicenseRef", "LicenseRef")
    identifier = identifier.replace(" AND NOASSERTION", "")
    words = identifier.split()
    for i, word in enumerate(words):
        for search, replace in GPL_RENAME.items():
            if word == search:
                words[i] = replace
                word = replace # fix LPGL->LGPL
        for gpl_lic in GPL_LICS:
            if word == f"LicenseRef-{gpl_lic}":
                words[i] = gpl_lic
    identifier = " ".join(words)
    return SPDXLicense.from_identifier(identifier)

def fix_fossy_spdx(doc):
	pkg = doc.package
	pkg.conc_lics = fix_fossy_lic(pkg.conc_lics)
	pkg.license_declared = fix_fossy_lic(pkg.license_declared)
	if isinstance(pkg.licenses_from_files, list):
		for i, l in enumerate(pkg.licenses_from_files):
			pkg.licenses_from_files[i] = fix_fossy_lic(l)
	if isinstance(pkg.files, list):
		for f in pkg.files:
			f.conc_lics = fix_fossy_lic(f.conc_lics)
			if isinstance(f.licenses_in_file, list):
				for i, l in enumerate(f.licenses_in_file):
					f.licenses_in_file[i] = fix_fossy_lic(l)
	if (
		hasattr(doc, "extracted_licenses")
		and isinstance(doc.extracted_licenses, list)
	):
		to_remove = [ f"LicenseRef-{id}" for id in GPL_LICS ] + TO_REMOVE
		doc.extracted_licenses = [
			el for el in doc.extracted_licenses
			if el.identifier not in to_remove
		]

def parse_spdx_tv(spdx_tv_fname):
    p = SPDXTagValueParser(SPDXTagValueBuilder(), SPDXLogger())
    p.build()
    with open(spdx_tv_fname, 'r') as f:
        doc, error = p.parse(f.read())
    return doc, error

def write_spdx_tv(spdx_doc_obj, filename):
	with open(filename, "w") as f:
		tv_write_document(spdx_doc_obj, f, validate=False)

if __name__ == "__main__":
    infile = sys.argv[1]
    fname, ext = os.path.splitext(infile)
    outfile = f"{fname}.fixed{ext}"
    doc, _ = parse_spdx_tv(infile)
    fix_fossy_spdx(doc)
    write_spdx_tv(doc, outfile)
