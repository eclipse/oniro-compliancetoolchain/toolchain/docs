---
SPDX-License-Identifier: CC-BY-SA-4.0 AND LicenseRef-Trademark-Notice
SPDX-FileCopyrightText: NOI Techpark SpA
---

# Oniro Compliance Toolchain and Audit Workflow - Documentation

- Introduction:
  - [What Oniro is all about](audit_workflow/what_Oniro_is_all_about.md) (read me first)
  - [Getting started](deployment/getting_started.md) (toolchain development and deployment)
- [Audit Workflow description and guidelines](./audit_workflow)
- [Research and Development](./development)
- [Compliance Toolchain Deployment](./deployment)
- [Demo](./demo)
