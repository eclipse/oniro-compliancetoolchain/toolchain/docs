# Processing Time Statistics

Aliens4Friends workflow has been tested on a VM with **16 VCPU**, **32 GB RAM**, and **two virtual disks** (system **160GB**, data **1TB**), by processing all flavors, machines and images of the first release of OpenHarmonyOS v0.1.0, built with the attached script [`./build_all.sh`](./build_all.sh). This test included all machines (and not only `quemu*` boards, like the test displayed in the demo video clips), in order to get more precise processing time statistics.

The A4F workflow scanned **from scratch**  **229 source packages** (from bitbake recipes), consisting of a total of **800,923 source files**.

As for Fossology (which is a *separate* tool, where sources are uploaded in order to be reviewed by an audit team), a *separate* VM with 16 VCPU, 32GB RAM and a 300GB virtual disk is being used, even if such horsepower is not needed to run the workflow, but to allow the audit team to work with Fossology WebUI even if some automated jobs, scheduled by aliens4friends, are running in the background on the same machine.

The following are the processing times for each step of the workflow. It is also specified which steps/subcommands can make use of multiprocessing, thus leveraging all the 16 cores available.

| subcommand       | multiproc. | exec time (h:m:s) |
| ---------------- |:----------:| -----------------:|
| tinfoilhat       |     no     |          00:49:56 |
| aliensrc_creator |    yes     |          00:32:12 |
| add              |     no     |          00:00:06 |
| match            |    yes     |          00:06:56 |
| scan             |    yes     |          12:35:26 |
| delta            |    yes     |          00:01:14 |
| spdxdebian       |    yes     |          00:05:53 |
| spdxalien        |    yes     |          00:08:13 |
| upload (*)       |     no     |          17:00:34 |
| fossy            |     no     |          01:36:15 |
| harvest          |     no     |          00:00:58 |
| **TOTAL**        |            |      **32:57:43** |


> (*) The separate Fossology VM had some performance issues during the upload stage that slowed down the whole process at some point in time due to VM infrastructure problems (the same job suddenly fell down from 365 items/sec to 3.627 items/sec -- ~100 times slower! -- and then, after a few hours, it resumed original speed). So in the future we expect better results here.

The above processing time stats refer to a **full scan from scratch of more that 800,000 source files**.

Aliens4Friends is designed to **store all its intermediate results in a cache pool**, so once a package has been scanned, it need not to be scanned again; if such scans are run regularly during software development, it is reasonable to expect that **the time to scan  only the delta of changes will be significantly lower every time**.

The only two steps that need to be frequently repeated over time (to **update stats on audit work progress**) are the last two ones (`fossy` and `harvest` which take, for 229 packages, **about 1 hour and a half**).
