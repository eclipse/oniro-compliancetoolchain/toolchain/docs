---
# open with reveal-md 4.x.x
template: assets/theme/array_white.html
css:
  - copyright.css
  - assets/theme/custom_ostc.css
preprocessor: assets/js/shortcuts.js
verticalSeparator: ^----
language: en
revealOptions:
  transition: 'slide'
---

## Aliens4Friends: <!--center-->

# Why and What <!--center-->

---

## The Problem (or, why we need a toolchain)

The **problem** of Sotware Composition Analysis (SCA) when you *don't* have a package manager (like with Yocto/Bitbake):

- <!--frag--> Aliens from another planet in a pool party
- <!--frag--> Who to invite?
- <!--frag--> They may bring problems (legal, security)
  - A background check is needed
  - Or trusted friends vouch for them

---

## The Solution: Tools

Tools
  - to find license and copyright notices *within* the code
      - *ScanCode*
      - *Fossology's scanners*
  - and to review them
      - *Fossology WebUI*

---

## Fossology

- what it does and what it help us to do
- what it *doesn't* do:
    - **code snippets?** yes, but it's no anti-plagiarism tool
- it's *not* a comprehensive tool:
  - needs input (source packages) from some other tool
  - some other tool has to collect output, generate SBOM and elaborate stats

---

## Fossology: the Problem

- Fossology requires a lot of human work (auditors)
  - hundreds of packages, hundreds of thousands of files
  - hundreds of man-hours (auditing)
<!-- FIXME add a slide with a Fossology screenshot here? -->
-<!--frag--> Open Source means avoiding reinventing the wheel and reusing others' work:
  - can we save some (or a lot) of work by **reusing** someone else's work? Someone trusted, though

---

## the Solution: Debian Matching

- Debian is like a **trusted "friend"** that vouches for the **"alien" packages**
- reuse copyright/license information which has already been collected and maintained by **humans@Debian**, and are machine readable (DEP5)
- DEP5 specs: every file must have a copyright and a license in the `debian/copyright` file of the debian package
- `debian/copyright` is machine readable, we can reuse all metadata!

---

## the Solution: Debian Matching

- it does not solve everything:
  - not always a full match in Debian
  - not all packages may be found in Debian
  - not all debian/copyright files are machine readable :(
-<!--frag--> but it really helps and **saves a substantial amount of human work**
<!-- FIXME show the dashboard here? -->

---

## Another possible "REUSE", to save even more work

- Some packages are fully or partially REUSE compliant:
  - what does that mean?
  - reusing information coming directly from the package maintainer
  - <!--frag--> a dedicated Fossology agent (`ojo`) can do that for us!

---

## What does the toolchain do?

1) provide input (source packages) to Fossology

2) integrate ScanCode as an additional scanner for Fossology -> provide **more accurate findings**

3) find matching debian packages and import their license and copyright metadata into Fossology -> **save human work**

4) use `ojo` for REUSE-compliant packages -> **save human work**

5) collect/elaborate output from Fossology during audit work:
   - **SBOM** in SPDX format
   - elaborate stats on **progress and results** of the audit work and show them in a **dashboard**

---

## Coming next...

# Aliens4Friends, How?

*Stay tuned!*
