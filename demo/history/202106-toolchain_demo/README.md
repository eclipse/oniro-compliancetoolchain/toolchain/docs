# Toolchain Demo

Presentation slides and demo video for the OSS Compliance Toolchain (Aliens4Friends).

Instructions on how deploy a demo environment for Aliens4Friends (like the one showed in the videos below) may be found in the [`./machine`](./machine) folder.

## Aliens4Friends: Why and What

<table style="width:100%">
  <tr>
    <th>
      <a href="video/a4f_intro_why_what.mp4"><img src="video/thumbnails/a4f_intro_why_what.png" alt=""></a>
    </th>
  </tr>
</table>

## Aliens4Friends: How

<table style="width:100%">
  <tr>
    <th>
      <img src="video/thumbnails/a4f_how_hands-on.png" alt="" width="320">
    </th>
    <th>
      <a href="<img/aliens4friends-schema.svg"><img src="img/aliens4friends-schema.png" alt="" width="320"></a>
    </th>
  </tr>
  <tr>

  <tr>
    <th>
      <a href="video/a4f_how_hands-on_1.mp4"><img src="video/thumbnails/a4f_how_hands-on_1.png" alt="" width="320"></a>
    </th>
    <th>
      <a href="video/a4f_how_hands-on_2.mp4"><img src="video/thumbnails/a4f_how_hands-on_2.png" alt="" width="320"></a>
    </th>
  </tr>
  <tr>
  <tr>
    <th>
      <a href="video/a4f_how_hands-on_3.mp4"><img src="video/thumbnails/a4f_how_hands-on_3.png" alt="" width="320"></a>
    </th>
    <th>
      <a href="video/a4f_how_hands-on_4.mp4"><img src="video/thumbnails/a4f_how_hands-on_4.png" alt="" width="320"></a>
    </th>
  </tr>
  <tr>
    <th>
      <a href="video/a4f_how_hands-on_5.mp4"><img src="video/thumbnails/a4f_how_hands-on_5.png" alt="" width="320"></a>
    </th>
    <th>
      <a href="video/a4f_how_hands-on_6.mp4"><img src="video/thumbnails/a4f_how_hands-on_6.png" alt="" width="320"></a>
    </th>
  </tr>
  <tr>
  <tr>
    <th>
      <a href="video/a4f_how_hands-on_7.mp4"><img src="video/thumbnails/a4f_how_hands-on_7.png" alt="" width="320"></a>
    </th>
    <th>
      <a href="video/a4f_how_hands-on_8.mp4"><img src="video/thumbnails/a4f_how_hands-on_8.png" alt="" width="320"></a>
    </th>
  </tr>

</table>

## Aliens4Friends: Results

<table style="width:100%">
  <tr>
    <th>
      <a href="video/a4f_results.mp4"><img src="video/thumbnails/a4f_results.png" alt=""></a>
    </th>
  </tr>
</table>
