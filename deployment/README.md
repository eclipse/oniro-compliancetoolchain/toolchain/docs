---
SPDX-License-Identifier: CC-BY-SA-4.0 AND LicenseRef-Trademark-Notice
SPDX-FileCopyrightText: NOI Techpark SpA
---

# Documentation about Oniro Compliance Toolchain deployment

Given the ongoing process of defining a migration path of the toolchain in EF infrastructure, this documentation should be moslty considered as WIP.

- [Getting started](./getting_started.md)
- Migration to EF infrastructure
  - [Migration plans](./migration_plans.md)
  - [Brainstorming](./brainstorming.md)
- Fossology:
  - [installation script](fossology/installation)
  - [migration plan](fossology/migration)
  - [past upgrade activity logs](fossology/upgrade)
