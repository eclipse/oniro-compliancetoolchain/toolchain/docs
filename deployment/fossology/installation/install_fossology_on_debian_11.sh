#!/bin/bash
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: 2020-2022 Alberto Pianon <pianon@array.eu>
#
# Simple script to install Fossology from sources in Debian 10

DEFAULT_RELEASE="4.4.0"

fossy_release="${FOSSY_RELEASE:-$DEFAULT_RELEASE}"

cp /etc/os-release .
chmod +x os-release
. "./os-release"
rm os-release
if [[ "$NAME $VERSION" != "Debian GNU/Linux 11 (bullseye)" ]]; then
  echo "This script must be run only in Debian 11"
  exit 1
fi

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

set -e

echo ""
echo ""
echo "***************************************************"
echo "*            INSTALLING SCRIPT DEPS...            *"
echo "***************************************************"
apt update
apt install -y sudo build-essential git pkg-config libpq-dev libglib2.0-dev \
  mc mawk sed software-properties-common lsb-release php7.4-cli \
  cmake ninja-build curl cron  python python3 python3-yaml python3-psycopg2 \
  python3-requests python3-pip
apt-add-repository non-free
apt update
apt install -y unrar

echo ""
echo ""
echo "***************************************************"
echo "*          MAKING SURE THAT en_US.UTF-8           *"
echo "*          LOCALE IS GENERATED AND SET            *"
echo "***************************************************"
locale-gen en_US.UTF-8
update-locale LANG=en_US.UTF-8

echo ""
echo ""
echo "***************************************************"
echo "*            CLONING FOSSOLOGY REPO...            *"
echo "***************************************************"
git config --global user.email "fossy@example.com"
git config --global user.name "Fossy"
cd /
git clone https://github.com/fossology/fossology.git
cd fossology/
git checkout tags/$fossy_release
# https://github.com/fossology/fossology/pull/2697
git cherry-pick 31ac4128d7986d0b07de764dcd6b6b173814080c
# https://github.com/fossology/fossology/pull/2707
git cherry-pick 56dedaa49ef649178b5d4d3b3ff78ee00ce3cc2d

echo ""
echo ""
echo "***************************************************"
echo "*                    CLEANING...                  *"
echo "***************************************************"
utils/fo-cleanold

echo ""
echo ""
echo "***************************************************"
echo "*           INSTALLING FOSSOLOGY DEPS...          *"
echo "***************************************************"
utils/fo-installdeps -y -e
apt-get install php-uuid

echo ""
echo ""
echo "***************************************************"
echo "*            COMPILING FOSSOLOGY...               *"
echo "***************************************************"
cmake -S. -B./build -DTESTING=ON -G Ninja
cmake --build ./build --parallel

echo ""
echo ""
echo "***************************************************"
echo "*            INSTALLING FOSSOLOGY...              *"
echo "***************************************************"
cmake --install ./build


echo ""
echo ""
echo "***************************************************"
echo "*              POST INSTALL STUFF...              *"
echo "***************************************************"
python3 -m pip install pip==21.2.2
pip3 install --force-reinstall importlib-metadata==8.0.0
/usr/local/lib/fossology/fo-postinstall

a2enmod ssl
a2ensite default-ssl

cd /etc/apache2/sites-available/
mv fossology.conf fossology.conf.bak
awk -vRS="AllowOverride None" -vORS="AllowOverride None\n\tSSLRequireSSL" '1' \
 fossology.conf.bak | head -n -2 > fossology.conf
service apache2 restart


if [[ -n "$FOSSY_ENABLE_PHPPGADMIN" ]]; then
  echo ""
  echo ""
  echo "***************************************************"
  echo "*              INSTALLING PHPPGADMIN...           *"
  echo "***************************************************"

  cd /tmp
  # phppgadmin is not available on Debian 11 anymore (why?), so we need to
  # download it from Debian 12
  wget http://ftp.de.debian.org/debian/pool/main/p/phppgadmin/phppgadmin_7.14.7+dfsg-1~bpo12+1_all.deb
  apt install -y ./phppgadmin_7.14.7+dfsg-1~bpo12+1_all.deb
  cd -

  cd /etc/apache2/conf-available/
  mv phppgadmin.conf phppgadmin.conf.bak
  sed -e 's/Require local/# Require local/' phppgadmin.conf.bak | \
  awk -vRS="<Directory /usr/share/phppgadmin>"  \
      -vORS="<Directory /usr/share/phppgadmin>\nSSLRequireSSL" '1' | \
  head -n -2 > phppgadmin.conf
  service apache2 restart
  cd -
fi

echo ""
echo ""
echo "***************************************************"
echo "*              TUNING SERVER CONFIG...            *"
echo "***************************************************"

# https://github.com/fossology/fossology/wiki/Configuration-and-Tuning#adjusting-the-kernel
page_size=`getconf PAGE_SIZE`
phys_pages=`getconf _PHYS_PAGES`
shmall=`expr $phys_pages / 2`
shmmax=`expr $shmall \* $page_size`
echo kernel.shmmax=$shmmax >> /etc/sysctl.conf
echo kernel.shmall=$shmall >> /etc/sysctl.conf


#https://github.com/fossology/fossology/wiki/Configuration-and-Tuning#preparing-postgresql
mem=$(free --giga | grep Mem | awk '{print $2}')
su - postgres -c psql <<EOT
ALTER SYSTEM set shared_buffers = '$(( mem / 4 ))GB';
ALTER SYSTEM set effective_cache_size = '$(( mem / 2 ))GB';
ALTER SYSTEM set maintenance_work_mem = '$(( mem * 50 ))MB';
ALTER SYSTEM set work_mem = '128MB';
ALTER SYSTEM set fsync = 'on';
ALTER SYSTEM set full_page_writes = 'off';
ALTER SYSTEM set log_line_prefix = '%t %h %c';
ALTER SYSTEM set standard_conforming_strings = 'on';
ALTER SYSTEM set autovacuum = 'on';
EOT

#https://github.com/fossology/fossology/wiki/Configuration-and-Tuning#configuring-php
/fossology/install/scripts/php-conf-fix.sh --overwrite

PHP_PATH=$(php --ini | awk '/\/etc\/php.*\/cli$/{print $5}')
phpIni="${PHP_PATH}/../apache2/php.ini"
sed \
  -i.bak \
  -e "s/upload_max_filesize = 700M/upload_max_filesize = 1000M/" \
  -e "s/post_max_size = 701M/post_max_size = 1004M/" \
  -e "s/memory_limit = 702M/memory_limit = 3030M/" \
  $phpIni

#https://github.com/fossology/fossology/wiki/Email-notification-configuration#setting-up-the-email-client
# but see also https://github.com/fossology/fossology/issues/1614
apt install s-nail
ln -s /usr/bin/s-nail /usr/bin/mailx

echo ""
echo ""
echo "***************************************************"
echo "* FIXING ISSUE WITH SCANCODE AND MONKBULK AGENTS  *"
echo "***************************************************"

sed -i -E 's/^special\[\] =/special[] = NOKILL/' /usr/local/share/fossology/scancode/scancode.conf

sed -i -E 's/^special\[\] =/special[] = NOKILL/' /usr/local/share/fossology/monkbulk/monkbulk.conf

echo ""
echo ""
echo "***************************************************"
echo "*      IMPLEMENTING WORKAROUND TO ALLOW           *"
echo "*      TO RESTART SCHEDULER VIA WebUI|API         *"
echo "***************************************************"

echo "
#!/bin/bash
set -e
service fossology stop
sleep 2
service fossology start
" > /usr/sbin/restart_fossology.sh

chmod +x /usr/sbin/restart_fossology.sh
echo "www-data ALL=(ALL) NOPASSWD: /usr/sbin/restart_fossology.sh" > /etc/sudoers.d/fossology-restart

cd /usr/local/share/fossology/www/ui/
patch -p1 << EOT
--- a/admin-scheduler.php
+++ b/admin-scheduler.php
@@ -35,8 +35,8 @@
       "agents" => array(_("Agents"), _("Show a list of enabled agents.")),
       "verbose" => array(_("Verbose"), _("Change the verbosity level of the scheduler or a job.")),
       "stop" => array(_("Shutdown Scheduler"), _("Shutdown the scheduler gracefully and stop all background processing.  This can take a while for all the agents to quit.")),
-      //    "start" => array(_("Start Scheduler"), _("Start Scheduler.")),
-      //    "restarts" => array(_("Restart Scheduler"), _("Restart Scheduler.")),
+      "start" => array(_("Start Scheduler"), _("Start Scheduler.")),
+      "restarts" => array(_("Restart Scheduler"), _("Restart Scheduler.")),
       "restart" => array(_("Unpause a job"), _("Unpause a job.")),
       "pause" => array(_("Pause a running job"), _("Pause a running job.")),
       "priority" => array(_("Priority"), _("Change the priority of a job."))
@@ -197,9 +197,9 @@
   function StartScheduler(\$operation = '')
   {
     if (\$operation) {
-      \$command = "/etc/init.d/fossology restart >/dev/null 2>&1";
+      \$command = "sudo /usr/sbin/restart_fossology.sh >/dev/null 2>&1";
     } else {
-      \$command = "/etc/init.d/fossology start >/dev/null 2>&1";
+      \$command = "sudo /usr/sbin/restart_fossology.sh >/dev/null 2>&1";
     }
     \$lastline = system(\$command, \$rc);
     if (\$rc) {
EOT

cd -

echo ""
echo ""
echo "***************************************************"
echo "*         ENABLING FOSSOLOGY SCHEDULER...         *"
echo "***************************************************"

systemctl daemon-reload
systemctl enable --now fossology
service fossology restart
su - www-data -s /bin/bash -c 'sudo /usr/sbin/restart_fossology.sh'

echo ""
echo ""
echo "***************************************************"
echo "*                      DONE!                      *"
echo "***************************************************"
echo
read -p "hit enter to reboot system"

reboot
