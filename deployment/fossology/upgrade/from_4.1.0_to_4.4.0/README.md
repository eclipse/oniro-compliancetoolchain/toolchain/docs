---
title: "Upgrade from Fossology 4.1.0 to 4.4.0: activity log and report"
author: Alberto Pianon <pianon@array.eu>
date: "2024-02-12"
lastmod: "2024-05-06"
SPDX-License-Identifier: CC-BY-SA-4.0 AND LicenseRef-Trademark-Notice
SPDX-FileCopyrightText: Alberto Pianon <pianon@array.eu>
---

# Upgrade from Fossology 4.1.0 to 4.4.0: activity log and report

- [x] Check if installation/Upgrade instructions in the official wiki changed since the last time
- [x] check all the customizations/fixes added to the current instance (also manually added ones): have they been implemented in the latest release? (check also for db tables changes - eg copyright?)
- [x] create a backup of the current instance (only database and configuration files, but including php and postgres config files)
- [x] upgrade Debian 10 -> 11
- [x] do the upgrade
- [x] Check if system configuration tweaks are still applied
- [x] verify and check if everything works correctly
- [ ] Upgrade fossology-python package in aliens4friends (include it in docker image) and update and test api calls in a4f code
- [ ] implement missing customizations (if any): eg. job status API

## 1. Check if installation/Upgrade instructions in the official wiki changed since the last time

Yes, because Fossology switched to CMake. New installation/upgrade instructions:

```bash
cd /fossology
utils/fo-cleanold
utils/fo-installdeps
cmake -S. -B./build -DTESTING=ON -G Ninja
cmake --build ./build --parallel
cmake --install ./build
/usr/local/lib/fossology/fo-postinstall
systemctl daemon-reload
systemctl enable --now fossology
service fossology restart
```


# 2. check all the customizations/fixes added to the current instance (also manually added ones)

## 2.1 Code changes

| our manual change to 4.1.0 | implemented in 4.4.0 |
| ---------------------------| -------------------- |
| PATCHING REST API to correctly report job status (see [issue](https://github.com/fossology/fossology/issues/1800#issuecomment-71291978)) | yes (?), apparently it should be fixed by API code refactoring and particularly by [this PR](https://github.com/fossology/fossology/pull/2307) offering a route to get the history of all jobs of an upload |

## 2.2. DB changes

None relevant, apparently; only license IDs fixes for SPDX compliance (see [dbmigrate_4.2-4.3.php](https://github.com/fossology/fossology/blob/master/install/db/dbmigrate_4.2-4.3.php)))

## 2.3 System configuration

| config | applicable also to 4.4.0 |
| ------ | ------ |
| [Adjusting the Kernel](https://github.com/fossology/fossology/wiki/Configuration-and-Tuning#adjusting-the-kernel) | yes |
| [Tweaking postgresql](https://github.com/fossology/fossology/wiki/Configuration-and-Tuning#preparing-postgresql) | yes |
| [PHP configuration](https://github.com/fossology/fossology/wiki/Configuration-and-Tuning#configuring-php) (see [here](../../installation/install_fossology_on_debian_10.sh) line 155) | yes |
| mail configuration (see [here](../../installation/install_fossology_on_debian_10.sh) line 167) | yes |

# 3. create a backup of the current instance (only database and configuration files, but including php and postgres config files)

```bash
PHP_PATH=$(php --ini | awk '/\/etc\/php.*\/cli$/{print $5}')
phpIni="$(realpath ${PHP_PATH}/../apache2/php.ini)"

tar czvf 20240212_fossology_code_and_config_backup.tgz \
 /var/local/lib/fossology \
 /var/local/cache/fossology \
 /usr/local/etc/fossology \
 /usr/local/lib/fossology \
 /usr/local/share/fossology \
 $phpIni \
 /etc/postgresql/11/main \
 /var/lib/postgresql/11/main/postgresql.auto.conf

su - postgres -c 'pg_dumpall' | gzip > 20240212_fo_dbbackup.gz
# create also an internal copy

service fossology stop
service apache2 stop
su - postgres -c psql
CREATE DATABASE fossologybak20240212 WITH TEMPLATE fossology OWNER fossy;
\q
```


# 4. install/upgrade [FAILED]

```bash
cd /fossology
git stash
git pull origin 4.4.0
```

```bash
utils/fo-cleanold
```

log:

```
*** Searching for old fossology config files ***
*** Searching for new fossology config files ***
WARNING: found /etc/cron.d/fossology but not deleting
WARNING: found /etc/init.d/fossology but not deleting
WARNING: found /usr/local/etc/fossology/Db.conf but not deleting
WARNING: found /usr/local/etc/fossology/fossology.conf but not deleting
WARNING: found /usr/local/etc/fossology/VERSION but not deleting
WARNING: found /usr/local/etc/fossology/samplefooter.txt but not deleting
WARNING: found /usr/local/etc/fossology/sampleheader.txt but not deleting
*** Cleaning up old fossology files ***
*** Cleaning up old cron job for www-data and fossy user ***
no crontab for www-data
no crontab for fossy
```

```bash
utils/fo-installdeps
```

log (excerpt):

```
[...]
The following NEW packages will be installed:
 cmake cmake-data libcppunit-1.14-0 libcppunit-dev libomp-dev librhash0 libuv1 libzstd-dev ninja-build
0 upgraded, 9 newly installed, 0 to remove and 1 not upgraded.
[...]
The following NEW packages will be installed:
 php7.3-sqlite3
0 upgraded, 1 newly installed, 0 to remove and 1 not upgraded.
[...]
The following NEW packages will be installed:
 zstd
0 upgraded, 1 newly installed, 0 to remove and 1 not upgraded.
[...]
```

```bash
cmake -S. -B./build -DTESTING=ON -G Ninja
cmake --build ./build --parallel
cmake --install ./build
```

the last command returns the following warning:

```
Needs CMake version greater than 3.19.0 for packaging FOSSology. Skipping
```

and when trying to do

```bash
/usr/local/lib/fossology/fo-postinstall
```

it says:

```
-bash: /usr/local/lib/fossology/fo-postinstall: No such file or directory
```

None of the scripts supposed to be installed into `/usr/local/lib/fossology/` are installed, and directory is not even there

# 5. Debian upgrade

Let's upgrade debian from version 10 to version 11, because Debian 11 is the version used in Fossology's current docker image

## 5.1. System upgrade

```bash
apt update
apt dist-upgrade
nano /etc/apt/sources.list
```

change it to:

```text
deb http://cdn-aws.deb.debian.org/debian bullseye main non-free
deb-src http://cdn-aws.deb.debian.org/debian bullseye main non-free
deb http://security.debian.org/debian-security bullseye-security main non-free
deb-src http://security.debian.org/debian-security bullseye-security main non-free
deb http://cdn-aws.deb.debian.org/debian bullseye-updates main non-free
deb-src http://cdn-aws.deb.debian.org/debian bullseye-updates main non-free
deb http://cdn-aws.deb.debian.org/debian bullseye-backports main
deb-src http://cdn-aws.deb.debian.org/debian bullseye-backports main
```

then:

```bash
apt update
apt full-upgrade -y
# (select "yes" when asked to restart services, and keep existing config files)
systemctl reboot
```

## 5.2. Fix "Sudo: unable to resolve host" error:

```bash
nano /etc/hosts
```

add:

```
127.0.0.1 fossology-new
```

## 5.3. Manually upgrade postgresql 11 -> 13

(the distro upgrade didn't upgrade it and displayed a warning to upgrade it manually)

![](img/postgres-upgrade-warning.png)

```bash
apt install postgresql-13 postgresql-client-13

pg_dropcluster --stop 13 main
pg_upgradecluster 11 main
# it takes a very long time!

apt remove postgresql-11 postgresql-client-11
apt autoremove
systemctl daemon-reload
service postgresql restart
apt install phppgadmin
# need to reinstall phppgadmin after distro upgrade
```

# 6. Upgrade fossology (again!)


```bash

cd /
mv fossology/ fossology.bak
git clone https://github.com/fossology/fossology.git
cd fossology
git checkout 4.4.0
utils/fo-cleanold
```

log of the last command:

```
*** Searching for old fossology config files ***
*** Searching for new fossology config files ***
WARNING: found /etc/cron.d/fossology but not deleting
WARNING: found /etc/init.d/fossology but not deleting
WARNING: found /usr/local/etc/fossology/Db.conf but not deleting
WARNING: found /usr/local/etc/fossology/fossology.conf but not deleting
WARNING: found /usr/local/etc/fossology/VERSION but not deleting
WARNING: found /usr/local/etc/fossology/samplefooter.txt but not deleting
WARNING: found /usr/local/etc/fossology/sampleheader.txt but not deleting
*** Cleaning up old fossology files ***
*** Cleaning up old cron job for www-data and fossy user ***
no crontab for www-data
no crontab for fossy
```

then:

```bash
utils/fo-installdeps
apt autoremove
cmake -S. -B./build -DTESTING=ON -G Ninja
cmake --build ./build --parallel
cmake --install ./build
/usr/local/lib/fossology/fo-postinstall
systemctl daemon-reload
systemctl enable --now fossology
nano /usr/local/etc/fossology/Db.conf

```

put the right db password again, then:

```bash
service fossology restart
```

## 7. Check and re-apply configuration, if needed

Kernel shm values are slightly different in Debian 11 (new kernel?) so we re-set them:

```bash
cp /etc/sysctl.conf /etc/sysctl.conf.bak
cat /etc/sysctl.conf.bak | grep -v kernel.shmmax | grep -v kernel.shmall > /etc/sysctl.conf
rm /etc/sysctl.conf.bak
page_size=`getconf PAGE_SIZE`
phys_pages=`getconf _PHYS_PAGES`
shmall=`expr $phys_pages / 2`
shmmax=`expr $shmall \* $page_size`
echo kernel.shmmax=$shmmax >> /etc/sysctl.conf
echo kernel.shmall=$shmall >> /etc/sysctl.conf
```

Postgresql config seems fine, migration worked well.


Php configuration needs to be applied again, instead (there's a new php version):

```bash
/fossology/install/scripts/php-conf-fix.sh --overwrite
PHP_PATH=$(php --ini | awk '/\/etc\/php.*\/cli$/{print $5}')
phpIni="${PHP_PATH}/../apache2/php.ini"
sed \
 -i.bak \
 -e "s/upload_max_filesize = 700M/upload_max_filesize = 1000M/" \
 -e "s/post_max_size = 701M/post_max_size = 1004M/" \
 -e "s/memory_limit = 702M/memory_limit = 3030M/" \
 $phpIni
```

Fix "Sudo: unable to resolve host" error, again:

```bash
nano /etc/cloud/templates/hosts.debian.tmpl
```

add:

```
127.0.0.1 fossology-new
```

and then:

```bash
nano /etc/hosts
```

add the same line as above.

finally:

```bash
reboot
```

# 8. verify and check if everything works correctly

Apparently it works, but when opening any upload it displays a blank page. After enabling php debug in `/etc/apache2/sites-enabled/fossology.conf` (and restarting apache2), it displays the following error:


> Warning: pg_prepare(): Query failed: ERROR: column license_ref.rf_spdx_id does not exist LINE 2: FROM ( SELECT license_ref.rf_shortname, license_ref... ^ in /usr/local/share/fossology/lib/php/Db/Driver/Postgres.php on line 49
>
> Fatal error: Uncaught Fossology\Lib\Exception: error executing: SELECT rf_shortname AS license_shortname, rf_spdx_id AS spdx_id, rf_pk, count(*) AS count, count(distinct pfile_ref.pfile_fk) as "unique" FROM ( SELECT license_ref.rf_shortname, license_ref.rf_spdx_id, license_ref.rf_pk, license_file.fl_pk, license_file.agent_fk, license_file.pfile_fk FROM license_file JOIN license_ref ON license_file.rf_fk = license_ref.rf_pk) AS pfile_ref RIGHT JOIN uploadtree_a UT ON pfile_ref.pfile_fk = UT.pfile_fk AND agent_fk=ANY($4) WHERE (rf_shortname IS NULL OR rf_shortname NOT IN ('Void')) AND upload_fk=$1 AND (UT.lft BETWEEN $2 AND $3) AND UT.ufile_mode&(3<<\28)=0 GROUP BY license_shortname, spdx_id, rf_pk -- Fossology\Lib\Dao\LicenseDao::getLicenseHistogram.uploadtree_a.36,33,38 ERROR: column license_ref.rf_spdx_id does not exist LINE 2: FROM ( SELECT license_ref.rf_shortname, license_ref... ^ in /u in /usr/local/share/fossology/lib/php/Db/DbManager.php on line 133

Tried to re-run `/usr/local/lib/fossology/fo-postinstall` and it apparently solved the problem (db migration has been successfully performed).

The issue was due to the fact that in the existing installation the db password, stored in Db.conf, was not the default one ('fossy'). When installing/upgrading Fossology, Db.conf was rewritten with the default password. When running fo-postinstall an "unable to connect to database" error occurred, so I restored the original password in Db.conf and restarted Fossology: it apparently worked, but I didn't realize that I had to re-run fo-postinstall to perform the db migration which was stopped because of the db login error.

Opened an issue upstream: <https://github.com/fossology/fossology/issues/2683>

## 8.1. Upload zlib package and run all agents

Uploaded https://www.zlib.net/zlib-1.3.1.tar.gz and run all agents. All **succeded** but for Software Heritage (which however had never been tested even with the old version, so for now we ignore it; probably it is a request limit issue of SH's API).

**UPDATE**: when trying to upload many packages, occasionally the scancode agent gets killed and make the whole fossology scheduler hang. This issue had already been reported here: https://github.com/fossology/fossology/issues/2642

## 8.2. Download SPDXTV report

works


# 9. Fix the scancode agent issue

see https://github.com/fossology/fossology/issues/2642

```
nano /usr/local/share/fossology/scancode/scancode.conf
```

change the last line to:

```
special[] = NOKILL
```

# 10. Fix a similar issue with monkbulk agent:

```
nano /usr/local/share/fossology/monkbulk/monkbulk.conf
```

change the last line to:

```
special[] = NOKILL
```

## 11. Workaround to activate the scheduler restart functionality in REST API

Beacuse of [this issue](https://github.com/fossology/fossology/issues/2684) it may be required to restart the scheduler via REST API when it hangs (or before or after every upload, as a preventive measure).

However the scheduler restart functionality is commented out Fossology's code, probably because it was not working properly for permissions issues.

To activate it, first we need to create a restart script at `/usr/sbin/restart_fossology.sh` (all the following commands need to be run as root)):

```bash
#!/bin/bash

set -e
service fossology stop
sleep 2
service fossology start
```

(we do not use `service fossology restart` because it may not work properly; we stop the service, wait a bit, then start it again).

Then we need to change the permissions of the script:

```bash
chmod +x /usr/sbin/restart_fossology.sh
```

Then we need to allow the www-data user to run the script with sudo without password:

```bash
echo "www-data ALL=(ALL) NOPASSWD: /usr/sbin/restart_fossology.sh" > /etc/sudoers.d/fossology-restart
```

Now we check if we are able to run it:

```bash
su - www-data -s /bin/bash -c 'sudo /usr/sbin/restart_fossology.sh'
```

If it works, we can proceed.

We need to edit `/usr/local/share/fossology/www/ui/admin-scheduler.php`. First, we need to uncomment lines 38-39:

```php
      "start" => array(_("Start Scheduler"), _("Start Scheduler.")),
      "restarts" => array(_("Restart Scheduler"), _("Restart Scheduler.")),
```

then we need to change the start/restart commands in lines 200-202:

```php
    if ($operation) {
      $command = "sudo /usr/sbin/restart_fossology.sh >/dev/null 2>&1";
    } else {
      $command = "sudo /usr/sbin/restart_fossology.sh >/dev/null 2>&1";
    }
```

We keep the same command for both start and restart, because the `service fossology start` command may not work properly.


# 12. Backport of upstream fixes:

- We raised [this issue](https://github.com/fossology/fossology/issues/2693) that made the file info endpoint fail in REST API, and we got accepted our fix in this [PR](https://github.com/fossology/fossology/pull/2697). The corresponding patch has been backported to our Fossology instance

- We raised [this issue](https://github.com/fossology/fossology/issues/2715) that made spdx report generation occasionally fail, but that had been already been fixed by this [PR](https://github.com/fossology/fossology/pull/2707). The corresponding patch has been backported to our Fossology instance

# 13. Upgrade fossology-python package in aliens4friends

## 13.1. Check if the API has changed (fossology-python 1.5.0 -> 3.2.1)


| method or object | changed in latest version |
| ---------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------- |
| `fossology.list_jobs(upload=upload, page_size=2000)` | apparently the same |
| `fossology.upload_file()` | almost the same, only access_level *optional* input param now needs to be an object and not a string |
| `Job.queueDate`, `Job.status` | apparently the same |
| `fossology.rootFolder` | apparently the same |
| `fossology.create_folder(parent_obj, name)` | apparently the same |
| `fossology.list_uploads()` | apparently the same |
| `Upload` | almost the same, removed filesize and filesha1 (only hash), added assignee-related properties |
| `fossology.detail_folder(upload.folderid)` (returns folder obj) | same |
| `Folder` | same |
| `fossology.schedule_jobs(folder, upload, specs, wait=True)` | same |
| `fossology.upload_summary(upload)` | same |
| `Summary` | same |
| `fossology.session.get` (used to get licenses and raw json summary, not using python wrapper method) | same, also json output is the same |
| `fossology.api` | same |
| `fossology.generate_report(upload=upload,report_format=ReportFormat.SPDX2TV)` (returns rep_id) | same |
| `fossology.download_report(rep_id)` | same |

It seems that nothing has changed in the API, so we can safely upgrade the package in aliens4friends and test it.

ACTUALLY Summary json response has changed, so we need to update the code to handle the new format. TODO: file an issue upstream


The only problem is the python version supported by fossology-python which is 3.10, while in aliens4friends it's 3.8 (also for compatibility with scancode v3.2.3)

Scancode 3.2.3 cannot run on 3.10, but a special virtualenv can be created for scancode with python 3.8. This way, we can use the latest fossology-python version in aliens4friends.

## 13.2 Used WebUI Functionalities not exposed in API

| functionality | added to API in 4.4.0 | added to fossology-python 3.2.1 |
| ------------- | --------------------- | ------------------------------- |
| reportImport  | yes                   | no                              |
| rename upload | no :(                 | no                              |

As a temporary solution, we may keep using the WebUI for these functionalities, and switch to REST API when both will be fully implemented both in the API and in fossology-python.

## 13.3 Preliminary conclusions

The upgrade of fossology-python package in aliens4friends should not require modifications to the code, but some possible hidden differences in the API behavior and possible issues due to the upgrade to python 3.10 could be discovered only by testing it.


