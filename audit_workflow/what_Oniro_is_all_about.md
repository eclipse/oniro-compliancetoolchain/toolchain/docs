---
title: What Oniro is all about
author: Alberto Pianon <pianon@array.eu>
reviewer: Carlo Piana
date: "2022-05-18"
lastmod: "2022-05-18"
SPDX-License-Identifier: CC-BY-SA-4.0 AND LicenseRef-Trademark-Notice
SPDX-FileCopyrightText: Alberto Pianon <pianon@array.eu>
---


# 1. What Oniro is all about

## 1.1. The main value that the Oniro Project provides: a book of (good, tested) recipes

> "It works on my machine..." \
> "Then we'll ship your machine!" \
> And that is how docker was born.


The meme is not just a joke but it tells the truth: if you want to distribute an application that simply works on any platform, either you go crazy in testing your application in any possible target device/OS, or you distribute a whole environment (libraries, dependencies, or even an entire operating system) where you are sure your application works (and cannot break other things).
That is one of the main reasons why we have docker in cloud servers, snap in linux desktops, venv for python, npm for javascript, and so on and so forth.

However, this cannot work for IoT/embedded devices: compared to a cloud server or to a desktop PC, IoT devices have incredibly low CPU and memory resources, and simply cannot handle to keep a whole separated environment just to run a single application.

So for the embedded world we need another solution; and this is where the Oniro Project comes into place.

In order to develop a common platform able to seamlessly run applications on different target devices, the Oniro Project committers spend most of their time in configuring, compiling, testing and debugging stuff on many different hardware devices (also using dedicated hardware infrastructures and platforms like [LAVA](https://validation.linaro.org/)), in order to find -- for example -- the particular configuration parameter or patch that make a software component work on a certain hardware board. Maybe it is just a small detail in a configuration file, but it is the detail that makes the difference between a device that works and one that does not. And even if it is just a small detail (maybe even a single line in a configuration file), it might have taken hours or even days to find it and to test it.

So the Oniro Project is not (mainly) about developing software: actually, there is a part of software development (for system OTA management, application frameworks, blueprints, etc.), but the main part of the work (and of the provided value, from a technical standpoint) is about writing and testing recipes that can be used to make things work seamlessly on many different IoT devices.

It is not just a matter of good software selection and composition; it is about making that good software work in any IoT device.

The expression "writing recipes" above is not just a metaphor: actually, the Oniro Project leverages the Yocto build framework, which basically consists of a set of data formats and tools to write machine-readable recipes in order to be able "cook" working firmware images for embedded devices. Such recipes do not contain software components' source code, but just a set of configuration parameters, instructions and patches needed to fetch, patch, configure, compile and install software components.

So **the main part of the project is not about developing software components, but it is about writing and testing Yocto recipes**, which entail a significant testing and debugging work which can be leveraged by downstream adopters.

## 1.2. A book of recipes among other books of recipes

Given the design and nature of the Yocto project, nobody writes from scratch a full-stack "book" of Yocto recipes to cook a specific firmware image.

Yocto "books of recipes" are organized in layers: the Yocto project itself provides some base layers to build a reference Linux distribution for emulated hardware platforms; hardware vendors provide their own layer(s) in order to add support for their hardware components; application providers provide layers to build their applications; and so on and so forth.

Layers can be combined together, and each layer may have not only its own recipes, but also override or append stuff to another layer's recipes.

In the end, the final selection, composition, fetching, compilation and distribution of software components entirely lies with end-user device manufacturers -- who choose which "books of recipes" (layers) they want to use, which recipes they want to use inside those books and which not, and whether to use such recipes as such or to modify them.

The Oniro Project is not any different in this respect: it provides a set of Yocto layers of recipes that, combined with other Yocto layers, can be used to cook firmware images for embedded devices; it also provides an example/default project manifest (basically, a list of layers) and a set of instructions and configuration files to build some base operating system images, which are not intended to be used in "real" products, but can be used by device makers as a reference/example to start from in order to build "real" firmware images for end-user products.

## 1.3. It is not only the ingredients: it is how you mix them that makes the difference

Another key value that is developed and provided by the Oniro Project lies precisely in this combination of different Yocto layers coming from different sources, which requires (for the same reasons described before) a lot of testing and validation activities in order to allow such a combination to become a reference project that can actually and seamlessly *work* on different target devices. This entails also some forking activity of third party Yocto layer repositories, which may need some adjustments to work well together (again, it may be even a single line in some configuration file, but it may take a lot of time to find out that specific line that needs to be changed).

Last but not least, in the whole layer design, combination, build and testing process, **reproducibility** is key: any device maker wanting to adopt Oniro must be able to reproduce all the steps of the process, so the latter must be thoroughly documented, as well as the whole build and testing toolchain used to support it. It goes without saying that reproducibility is also important for any due diligence and IP compliance process a device maker may want to perform on its own projects based on Oniro.

## 1.4. Learning by example: Oniro Blueprints

Oniro reference images as such have no application layer, since they are intended to be like a blank screen where a device maker can design its own IoT products and add its own applications. However, in order to enable a device maker to do it, some working examples may be much more useful than tons of theoretical documentation.

This is why the Oniro Project involves also the development of **blueprints**, that is, sample applications working on an Oniro-based system (eg. a vending machine, a doorlock, etc.) with complete instructions (hardware components, cabling, etc.) on how to build a "real" sample device.

This last activity actually involves some software development activity, even if it is always intended to develop examples and not real products.

## 1.5. What Oniro is all about, in conclusion

In conclusion, **the Oniro Project is mainly about producing and distributing documentation, recipes and instructions** intended to be **used as a reference by device makers** to build firmware for their own devices. To a lower extent, the Oniro Project also involves the production or customization of some "ingredients" (i.e. software components, sample applications), but the latter is always instrumental to the main goal of the project, which is building a common open platform for interoperable IoT devices.

In the end, the overall meaning of the Oniro Project is, in the true open source way, avoid reinventing the wheel: instead of having each device maker assembling, testing, maintaining, securing and updating its own linux/zephyr/freeRTOS distribution(s) and application framework (which require skills, time and money that many device makers simply do not have, leading to unstable, insecure and under-maintained IoT devices), with Oniro the latter is developed as a community project everyone can contribute to -- so that device makers can concentrate and dedicate more resources to compete on the real value that they should provide: designing and developing good IoT applications in order to make everyone's life (hopefully) easier.

# 2. What Oniro *IP Compliance Process* is all about

## 2.1. Putting ourselves into the shoes of a device maker

As explained above, the Oniro project is not (mostly) about developing software, but about creating (or forking), combining and testing layers of Yocto recipes. Therefore, apart from some patches and scripts, most of the distributed "source code" *is not actually source code*, but metadata and configuration parameters, which may hardly -- if not at all -- reach the threshold for copyright protection (and therefore entail legal risks related to licensing or other copyright-related issues).

The actual source code of software components that are intended to be built and combined through the recipes is not included in the Oniro distribution. As explained above, the final selection, composition, fetching, compilation and distribution of software components (and the related liabilities and obligations) entirely lies with end-user device manufacturers, which will use Oniro just as a *reference* for their projects.

Hence, taking into account what is actually *distributed*, the legal risk related to the Oniro distribution *as such* is naturally low. There may be some possible legal issues (like for instance issues related to forked layers enabling support for specific hardware parts or devices, which at times may contain prebuilt binaries or firmware blobs with an unclear provenance or an unclear license) but, by and large, IP compliance activity on the Oniro project could be relatively easy, as long as no source or binary component/artifact is distributed.

But such an approach, although arguably correct from a legal standpoint, would be at odds with the spirit and the meaning of the whole project, deterring device makers from adopting it: every device maker would have to do its own IP compliance check on the same software components, thus duplicating the same work and reinventing the wheel every time -- but with less time and resources to invest than those a community-backed open source project may put in place.

So, even if it would not be strictly required from a legal point of view, we at Oniro decided to put ourselves into the shoes of a device maker who is willing to adopt Oniro, and to "simulate" the IP compliance work that it would be supposed to do in order to market its IoT device, without shortcuts. Such IP compliance work includes not only collection and validation of copyright and license metadata, but also, in perspective, license incompatibility checks, identification and review of license obligations that would apply *if* we distributed a real product, and the adoption and maintenance of an IP/Open Source Policy that may work also as a template for "real" device makers.

Of course it remains largely a simulation, since it cannot be performed on a real product but just on reference/sample firmware images built from a reference project manifest; but the work may be hopefully reused, in part or in full, by real device makers, who can then concentrate just on the compliance of the application layer they develop, and not of the whole linux/zephyr/freeRTOS distribution that support it (which usually consists of hundreds of standard software components).

## 2.2. Automating IP scanning of a complex build matrix: a dedicated toolchain

An IP compliance process on a whole embedded operating system distribution cannot be handled just by human "manual" work: it would be too costly, non-scalable, haphazard and impossible to keep in sync with a continuous software development process.

Automation is key in such context, so along with CI/CD (continuous integration / continuous delivery), a CC (Continuous Compliance) process must be put in place.

That is why we decided to use CI/CD pipelines and tools like Fossology and Scancode in order to routinely scan reference firmware images built from any new commit to the manifest and layer repositories, and to allow a dedicated IP Audit team to efficiently manage and balance workloads.

The challenge, compared to a regular Yocto project, is that we need to scan not only a single firmware image, but a whole matrix of firmware images that can be built depending on user's choices (linux, freeRTOS or zephyr kernel; target machine chosen; production or testing/debugging purpose; included component groups - base, extra, etc.). And the difference between one target and another may be not only the components used, but also the set of sources used to build the same components (f.e. the same upstream component may need different patches to work on different target machines).

The *post-mortem* source packaging functionalities provided by Yocto to perform software composition analysis are of little help in this context, since for the same upstream component a (slightly) different source package for each different target image would be generated, leading to a package proliferation that would be hardly manageable by the IP Audit Team.

That is why we had to develop a dedicated toolchain, aimed at aggregating source packages and metadata from the build matrix, and at presenting such data in a concise and comprehensive way in a nice graphical dashboard, that can be used to monitor and organize the audit work progress.[^dashboard]

[^dashboard]: In perspective, such dashboard should be used to collect and show also other information about software components used, not just related to IP compliance, but also to a component's open source project health condition (taking data from the [CHAOSS project](https://chaoss.community/)), security vulnerabilities, etc. The overall idea is to collect and aggregate any relevant data to evaluate each component used in Oniro, from any relevant perspective (maintenance, security, compliance, etc.)

## 2.3. Taking from and giving back to the community

In the true open source spirit, also the Oniro IP Compliance process has been designed having in mind the "avoid reinventing the wheel" principle -- therefore trying to reuse copyright and license metadata from reliable sources whenever possible.

As a first reliable source, we are importing metadata from the Debian project (whenever we find a closely matching component), mainly for two reasons: there is a huge work by a long-standing and well-established community behind it, and copyright and license metadata are generally provided in a machine readable format for every single source file (and not just for the whole software package, as in other Linux distributions).[^othersources]

[^othersources]: Other sources (eg. Fedora) may be added in the future.

Of course, open source is not just taking, but also giving back to the community. With the Oniro Compliance Project, this will happen in two ways.

First, we are planning to push upstream all fixes and corrections our Audit Team is making to any external copyright and license metadata we are reusing (from the Debian project, but also from projects that publish license metadata following the [REUSE standard](https://reuse.software/) like the Linux kernel).

Second (which is the whole sense of our work, as explained above), we will share copyright and license metadata reviewed by our Audit and Legal Teams (including identification and review of license obligations) so that they may be reused by device makers as a starting point to perform their due diligence process on their products based on Oniro. To this purpose, a database of such metadata will be made available through a public API and a Web Application, and possibly contributed also to external projects (like [ClearlyDefined](https://clearlydefined.io)).
